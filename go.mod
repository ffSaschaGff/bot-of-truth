module gitlab.com/ffSaschaGff/bot-of-truth

go 1.18

require (
	github.com/go-telegram/bot v0.7.8
	github.com/golang/mock v1.6.0
	github.com/segmentio/kafka-go v0.4.40
	github.com/stretchr/testify v1.8.1
	syreclabs.com/go/faker v1.2.3
)

require (
	github.com/klauspost/compress v1.15.14 // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	golang.org/x/net v0.5.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
