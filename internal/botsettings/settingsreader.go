// Copyright 2022 Galanov Aleksandr
// Licensed under the Apache License, Version 2.0

package botsettings

import (
	"encoding/json"
	"os"
)

// Settings - root of bot settings.
type settings struct {
	Events []*eventRaw   `json:"events"`
	Hooks  []*hookRaw    `json:"hooks"`
	Kafka  KafkaSettings `json:"kafka"`
}

// Event - event to trigger from new messages.
type eventRaw struct {
	Triggers       []*triggerRaw  `json:"triggers"`
	Responses      []*responseRaw `json:"responses"`
	RandonResponse bool           `json:"randomResponse"`
}

type KafkaSettings struct {
	Host          []string `json:"host"`
	MessagesTopic string   `json:"messages_topic"`
	Timeout       uint32   `json:"timeout_ms"`
	RetryTimeout  uint32   `json:"timeout_retry_ms"`
	RetryCount    int8     `json:"retry_count"`
	ConsumerGroup string   `json:"consumer_group"`
}

// Hook - hook to listen services for new messages.
type hookRaw struct {
	URL  string `json:"url"`
	Chat int64  `json:"chat"`
}

// Trigger - description when we have new event from telegram.
type triggerRaw struct {
	TriggerType string  `json:"type"`
	Content     string  `json:"content"`
	Chats       []int64 `json:"chats"`
}

// Response - description what to do in case of new event.
type responseRaw struct {
	Target      string `json:"target"`
	TriggerType string `json:"type"`
	Content     string `json:"content"`
	Method      string `json:"method"`
	URL         string `jsin:"url"`
	Message     string `jsin:"message"`
}

// ReadSettings - init settings from file.
func readSettings() *settings {
	plan, _ := os.ReadFile("./settings.json")
	var settings settings
	err := json.Unmarshal(plan, &settings)
	if err != nil {
		panic(err)
	}

	return &settings
}
