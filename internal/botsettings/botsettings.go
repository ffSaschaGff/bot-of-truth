package botsettings

import (
	"fmt"

	hookschecker "gitlab.com/ffSaschaGff/bot-of-truth/internal/hooks_checker"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/responses"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/triggers"
)

// BotBehavior - description of bot behavior.
type BotBehavior struct {
	Events []*Event
	Hooks  []*hookschecker.Hook
	Kafka  KafkaSettings
}

// Event - event needed to react.
type Event struct {
	Triggers       []triggers.Trigger
	Responses      []responses.Response
	RandonResponse bool
}

const selfTarget = "self"

// GetSettings - return settings based on readed file.
func GetSettings() BotBehavior {
	rawSettings := readSettings()
	readySettings := BotBehavior{
		Events: []*Event{},
		Hooks:  []*hookschecker.Hook{},
		Kafka:  rawSettings.Kafka,
	}
	for _, rawHook := range rawSettings.Hooks {
		hook := hookschecker.Hook{
			URL:  rawHook.URL,
			Chat: rawHook.Chat,
		}
		readySettings.Hooks = append(readySettings.Hooks, &hook)
	}
	for _, rawEvent := range rawSettings.Events {
		event := Event{
			Triggers:       []triggers.Trigger{},
			Responses:      []responses.Response{},
			RandonResponse: rawEvent.RandonResponse,
		}

		for _, rawResponse := range rawEvent.Responses {
			response := getResponse(rawResponse)
			event.Responses = append(event.Responses, response)
		}

		for _, rawTriger := range rawEvent.Triggers {
			triger := getTrigger(rawTriger)
			event.Triggers = append(event.Triggers, triger)
		}

		readySettings.Events = append(readySettings.Events, &event)
	}

	return readySettings
}

func getTrigger(rawTrigger *triggerRaw) triggers.Trigger {
	var trigger triggers.Trigger
	switch {
	case rawTrigger.TriggerType == "regexp":
		trigger = triggers.NewMessageRegexp(rawTrigger.Content, false)
	case rawTrigger.TriggerType == "unavoidableRegexp":
		trigger = triggers.NewMessageRegexp(rawTrigger.Content, true)
	case rawTrigger.TriggerType == "chats":
		trigger = triggers.NewChatTrigger(rawTrigger.Chats)
	case rawTrigger.TriggerType == "sticker":
		trigger = triggers.NewStickerTrigger(rawTrigger.Content)
	}

	return trigger
}

func getResponse(rawResponse *responseRaw) responses.Response {
	var response responses.Response

	triger := rawResponse.TriggerType

	switch triger {
	case "sticker":
		response = responses.NewStickerResponse(
			rawResponse.Content,
			rawResponse.Target == selfTarget,
		)
	case "voice":
		response = responses.NewVoiceResponse(
			rawResponse.Content,
			rawResponse.Target == selfTarget,
		)
	case "pin":
		response = responses.NewPinResponse()
	case "delete":
		response = responses.NewDeleteResponse()
	case "httpRequest":
		response = responses.NewHTTPResponse(
			responses.HTTPResponseDescription{
				Body:    rawResponse.Content,
				URL:     rawResponse.URL,
				Method:  rawResponse.Method,
				Message: rawResponse.Message,
			},
			rawResponse.Target == selfTarget,
		)
	case "text":
		response = responses.NewTextResponse(
			rawResponse.Content,
			rawResponse.Target == selfTarget,
		)
	default:
		message := fmt.Sprintf("can't find response %s", triger)
		logger.LogErr(message)
	}

	return response
}
