package responses

import (
	"context"

	telegram_api "github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
)

// Response - abstract response.
type Response interface {
	React(context.Context, Client, *models.Message) error
}

type Client interface {
	SendMessage(context.Context, *telegram_api.SendMessageParams) (*models.Message, error)
	PinChatMessage(ctx context.Context, params *telegram_api.PinChatMessageParams) (bool, error)
	SendSticker(ctx context.Context, params *telegram_api.SendStickerParams) (*models.Message, error)
	DeleteMessage(ctx context.Context, params *telegram_api.DeleteMessageParams) (bool, error)
	SendVoice(ctx context.Context, params *telegram_api.SendVoiceParams) (*models.Message, error)
}

type stickerResponse struct {
	stickerID  string
	selfTarget bool
}

type voiceResponse struct {
	vioceID    string
	selfTarget bool
}

type messageResponse struct {
	content    string
	selfTarget bool
}

type pinResponse struct{}

type deleteResponse struct{}

// NewStickerResponse - response by sticker
func NewStickerResponse(stickerID string, reply bool) Response {
	return &stickerResponse{
		stickerID:  stickerID,
		selfTarget: reply,
	}
}

// NewVoiceResponse - response by sin
func NewVoiceResponse(vioceID string, reply bool) Response {
	return &voiceResponse{
		vioceID:    vioceID,
		selfTarget: reply,
	}
}

// NewPinResponse - response by pinning msg
func NewPinResponse() Response {
	return &pinResponse{}
}

// NewDeleteResponse - response by delete message
func NewDeleteResponse() Response {
	return &deleteResponse{}
}

// NewTextResponse - response by text
func NewTextResponse(content string, reply bool) Response {
	return &messageResponse{
		content:    content,
		selfTarget: reply,
	}
}

func (response *deleteResponse) React(ctx context.Context, client Client, targetMessage *models.Message) error {
	_, err := client.DeleteMessage(
		ctx,
		&telegram_api.DeleteMessageParams{
			ChatID:    targetMessage.Chat.ID,
			MessageID: targetMessage.ID,
		},
	)

	return err
}

func (response *stickerResponse) React(ctx context.Context, client Client, targetMessage *models.Message) error {
	sticker := &telegram_api.SendStickerParams{
		ChatID:  targetMessage.Chat.ID,
		Sticker: &models.InputFileString{Data: response.stickerID},
	}
	if response.selfTarget {
		sticker.ReplyToMessageID = targetMessage.ID
	}

	_, err := client.SendSticker(
		ctx,
		sticker,
	)

	return err
}

func (response *voiceResponse) React(ctx context.Context, client Client, targetMessage *models.Message) error {
	voice := &telegram_api.SendVoiceParams{
		ChatID: targetMessage.Chat.ID,
		Voice:  &models.InputFileString{Data: response.vioceID},
	}
	if response.selfTarget {
		voice.ReplyToMessageID = targetMessage.ID
	}
	_, err := client.SendVoice(
		ctx,
		voice,
	)

	return err
}

func (response *pinResponse) React(ctx context.Context, client Client, targetMessage *models.Message) error {
	_, err := client.PinChatMessage(
		ctx,
		&telegram_api.PinChatMessageParams{
			ChatID:    targetMessage.Chat.ID,
			MessageID: targetMessage.ID,
		},
	)

	return err
}

func (response *messageResponse) React(ctx context.Context, client Client, targetMessage *models.Message) error {
	message := &telegram_api.SendMessageParams{
		ChatID: targetMessage.Chat.ID,
		Text:   response.content,
	}
	if response.selfTarget {
		message.ReplyToMessageID = targetMessage.ID
	}
	_, err := client.SendMessage(
		ctx,
		message,
	)

	return err
}
