package responses

import (
	"context"
	"testing"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/mocks"
)

func Test_deleteResponse_React(t *testing.T) {
	t.Parallel()

	currentTargetMessage := mocks.RandMessage()

	type args struct {
		client        Client
		targetMessage *models.Message
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "valid case",
			args: args{
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().DeleteMessage(
						context.Background(),
						&bot.DeleteMessageParams{
							ChatID:    currentTargetMessage.Chat.ID,
							MessageID: currentTargetMessage.ID,
						},
					).Return(
						true,
						nil,
					)
					return m
				}(),
				targetMessage: currentTargetMessage,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			response := NewDeleteResponse()
			err := response.React(
				context.Background(),
				tt.args.client,
				tt.args.targetMessage,
			)
			require.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func Test_stickerResponse_React(t *testing.T) {
	t.Parallel()

	currentTargetMessage := mocks.RandMessage()

	type fields struct {
		client        Client
		targetMessage *models.Message
	}
	type args struct {
		stickerID string
		reply     bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "valid case simple",
			fields: fields{
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().SendSticker(
						context.Background(),
						&bot.SendStickerParams{
							ChatID:  currentTargetMessage.Chat.ID,
							Sticker: &models.InputFileString{Data: "someID"},
						},
					).Return(
						&models.Message{},
						nil,
					)
					return m
				}(),
				targetMessage: currentTargetMessage,
			},
			args: args{
				stickerID: "someID",
			},
		},
		{
			name: "valid case simple",
			fields: fields{
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().SendSticker(
						context.Background(),
						&bot.SendStickerParams{
							ChatID:           currentTargetMessage.Chat.ID,
							Sticker:          &models.InputFileString{Data: "someID"},
							ReplyToMessageID: currentTargetMessage.ID,
						},
					).Return(
						&models.Message{},
						nil,
					)
					return m
				}(),
				targetMessage: currentTargetMessage,
			},
			args: args{
				stickerID: "someID",
				reply:     true,
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			response := NewStickerResponse(tt.args.stickerID, tt.args.reply)
			err := response.React(
				context.Background(),
				tt.fields.client,
				tt.fields.targetMessage,
			)
			require.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func Test_voiceResponse_React(t *testing.T) {
	t.Parallel()

	currentTargetMessage := mocks.RandMessage()

	type fields struct {
		client        Client
		targetMessage *models.Message
	}
	type args struct {
		vioceID string
		reply   bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "valid case simple",
			fields: fields{
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().SendVoice(
						context.Background(),
						&bot.SendVoiceParams{
							ChatID: currentTargetMessage.Chat.ID,
							Voice:  &models.InputFileString{Data: "someID"},
						},
					).Return(
						&models.Message{},
						nil,
					)
					return m
				}(),
				targetMessage: currentTargetMessage,
			},
			args: args{
				vioceID: "someID",
			},
		},
		{
			name: "valid case simple",
			fields: fields{
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().SendVoice(
						context.Background(),
						&bot.SendVoiceParams{
							ChatID:           currentTargetMessage.Chat.ID,
							Voice:            &models.InputFileString{Data: "someID"},
							ReplyToMessageID: currentTargetMessage.ID,
						},
					).Return(
						&models.Message{},
						nil,
					)
					return m
				}(),
				targetMessage: currentTargetMessage,
			},
			args: args{
				vioceID: "someID",
				reply:   true,
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			reaction := NewVoiceResponse(
				tt.args.vioceID,
				tt.args.reply,
			)
			err := reaction.React(
				context.Background(),
				tt.fields.client,
				tt.fields.targetMessage,
			)
			require.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func Test_messageResponse_React(t *testing.T) {
	t.Parallel()

	currentTargetMessage := mocks.RandMessage()

	type fields struct {
		client        Client
		targetMessage *models.Message
	}
	type args struct {
		content string
		reply   bool
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "valid case simple",
			fields: fields{
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().SendMessage(
						context.Background(),
						&bot.SendMessageParams{
							ChatID: currentTargetMessage.Chat.ID,
							Text:   "some text",
						},
					).Return(
						&models.Message{},
						nil,
					)
					return m
				}(),
				targetMessage: currentTargetMessage,
			},
			args: args{
				content: "some text",
			},
		},
		{
			name: "valid case simple",
			fields: fields{
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().SendMessage(
						context.Background(),
						&bot.SendMessageParams{
							ChatID:           currentTargetMessage.Chat.ID,
							Text:             "some text",
							ReplyToMessageID: currentTargetMessage.ID,
						},
					).Return(
						&models.Message{},
						nil,
					)
					return m
				}(),
				targetMessage: currentTargetMessage,
			},
			args: args{
				content: "some text",
				reply:   true,
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			reaction := NewTextResponse(
				tt.args.content,
				tt.args.reply,
			)
			err := reaction.React(
				context.Background(),
				tt.fields.client,
				tt.fields.targetMessage,
			)
			require.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func Test_pinResponse_React(t *testing.T) {
	t.Parallel()

	currentTargetMessage := mocks.RandMessage()

	type args struct {
		client        Client
		targetMessage *models.Message
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "valid case",
			args: args{
				targetMessage: currentTargetMessage,
				client: func() Client {
					m := NewMockClient(gomock.NewController(t))
					m.EXPECT().PinChatMessage(
						context.Background(),
						&bot.PinChatMessageParams{
							ChatID:    currentTargetMessage.Chat.ID,
							MessageID: currentTargetMessage.ID,
						},
					).Return(
						true,
						nil,
					)
					return m
				}(),
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			reaction := NewPinResponse()
			err := reaction.React(
				context.Background(),
				tt.args.client,
				tt.args.targetMessage,
			)
			require.Equal(t, tt.wantErr, err != nil)
		})
	}
}
