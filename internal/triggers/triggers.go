package triggers

import (
	"github.com/go-telegram/bot/models"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/messagechecker"
)

// Trigger - abstract trigger.
type Trigger interface {
	Check(*models.Message) bool
}

type chatTrigger struct {
	chats []int64
}

type regexpTrigger struct {
	checker *messagechecker.Checker
}

type stickerTrigger struct {
	stickerID string
}

// NewChatTrigger - trigger on chatID
func NewChatTrigger(chatIDs []int64) Trigger {
	return &chatTrigger{
		chats: chatIDs,
	}
}

// NewStickerTrigger - trigger on Sticker
func NewStickerTrigger(stickerID string) Trigger {
	return &stickerTrigger{
		stickerID: stickerID,
	}
}

// NewMessageRegexp - trigger on message text
func NewMessageRegexp(regexp string, unavoidable bool) Trigger {
	return &regexpTrigger{
		checker: messagechecker.GetCheker(regexp, unavoidable),
	}
}

func (trigger *chatTrigger) Check(targetMessage *models.Message) bool {
	for _, value := range trigger.chats {
		if value == targetMessage.Chat.ID {
			return true
		}
	}

	return false
}

func (trigger *regexpTrigger) Check(targetMessage *models.Message) bool {
	messageIsPositive := trigger.checker.CheckMessage(targetMessage.Text)
	captionIsPisitice := trigger.checker.CheckMessage(targetMessage.Caption)

	return messageIsPositive || captionIsPisitice
}

func (trigger *stickerTrigger) Check(targetMessage *models.Message) bool {
	return targetMessage.Sticker != nil && trigger.stickerID == targetMessage.Sticker.FileUniqueID
}
