package telegramclient

import (
	"context"
	"log"
	"os"

	telegram_api "github.com/go-telegram/bot"
)

type Client interface {
	Run(context.Context)
	GetAPI() *telegram_api.Bot
}

type client struct {
	api *telegram_api.Bot
}

func New(
	ctx context.Context,
	handler telegram_api.HandlerFunc,
) Client {
	token := os.Getenv("BOT_TELEGRAM_BOT_TOKEN")
	if token == "" {
		log.Fatal("cant find token in BOT_TELEGRAM_BOT_TOKEN env")
	}

	opts := []telegram_api.Option{
		telegram_api.WithDefaultHandler(handler),
	}

	api, err := telegram_api.New(token, opts...)
	if err != nil {
		panic(err)
	}

	return &client{
		api: api,
	}
}

func (c *client) Run(ctx context.Context) {
	c.api.Start(ctx)
}

func (c *client) GetAPI() *telegram_api.Bot {
	return c.api
}
