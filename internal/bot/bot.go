package bot

import (
	"context"
	"crypto/rand"
	"fmt"
	"math/big"
	"time"

	telegram_api "github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/botsettings"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/performance"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/responses"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/triggers"
)

// Bot - core of bot
type Bot struct {
	settings botsettings.BotBehavior
}

// NewBot - bot constructor
func NewBot(
	settings botsettings.BotBehavior,
) Bot {
	return Bot{
		settings: settings,
	}
}

func (b *Bot) Handle(ctx context.Context, bot *telegram_api.Bot, update *models.Update) {
	for _, event := range b.settings.Events {
		var targetMessage *models.Message

		if update.Message != nil {
			targetMessage = update.Message
		} else if update.EditedMessage != nil {
			targetMessage = update.EditedMessage
		}

		if targetMessage == nil {
			return
		}

		timeDelta := time.Now().Unix() - int64(targetMessage.Date)
		if timeDelta > 60*5 {
			return
		}

		metric := performance.Start()

		isPositive := checkTriggers(event.Triggers, targetMessage)

		if isPositive {
			pocessResponses(ctx, bot, targetMessage, event)
		}

		metricMessage := fmt.Sprintf("checking %d symbols message", len(targetMessage.Text))
		metric.End(metricMessage)
	}
}

func checkTriggers(triggers []triggers.Trigger, targetMessage *models.Message) bool {
	isPositive := true

	for _, trigger := range triggers {
		currentResult := trigger.Check(targetMessage)

		if !currentResult {
			isPositive = false

			break
		}

		isPositive = isPositive && currentResult
	}

	return isPositive
}

func pocessResponses(
	ctx context.Context,
	bot *telegram_api.Bot,
	targetMessage *models.Message,
	event *botsettings.Event,
) {
	if event.RandonResponse {
		optionsLen := len(event.Responses)
		index, err := rand.Int(rand.Reader, big.NewInt(int64(optionsLen)))
		if err != nil {
			logger.LogErr(err.Error())

			return
		}

		response := event.Responses[index.Int64()]
		processResponse(ctx, bot, response, targetMessage)
	} else {
		for _, response := range event.Responses {
			processResponse(ctx, bot, response, targetMessage)
		}
	}
}

func processResponse(
	ctx context.Context,
	bot *telegram_api.Bot,
	messageResponse responses.Response,
	targetMessage *models.Message,
) {
	err := messageResponse.React(ctx, bot, targetMessage)
	if err != nil {
		logger.LogErr(err.Error())
	}
}
