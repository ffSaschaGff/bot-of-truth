// Copyright 2022 Galanov Aleksandr
// Licensed under the Apache License, Version 2.0

package logger

import (
	"log"
	"os"
)

// LogErr - log error.
func LogErr(message string) {
	logMessage("ERROR\t", message, true)
}

// LogInfo - log info.
func LogInfo(message string) {
	logMessage("INFO\t", message, false)
}

// LogPerformance - log performance.
func LogPerformance(message string) {
	logMessage("PERFORMANCE\t", message, false)
}

// LogDebug - log debug message.
func LogDebug(message string) {
	isDebug := os.Getenv("BOT_DEBUG") == "y"

	if !isDebug {
		return
	}

	logMessage("DEBUG\t", message, false)
}

func logMessage(level string, message string, isError bool) {
	out := os.Stdout

	if isError {
		out = os.Stderr
	}

	logger := log.New(out, level, log.Ldate|log.Ltime)
	logger.Println(message)
}
