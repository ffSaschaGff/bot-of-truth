package hookschecker

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"

	telegram_api "github.com/go-telegram/bot"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/interfaces"
	"gitlab.com/ffSaschaGff/bot-of-truth/internal/logger"
)

// Hook - type for hook listener description.
type Hook struct {
	URL  string
	Chat int64
}

// EventForBot - input object to recive from others services.
type EventForBot struct {
	Type    string `json:"type"`
	Message string `json:"message"`
}

const (
	TypeMessage = "message"
)

const checkDelay = 5

// Watch - start watching hook.
func (hook Hook) Watch(
	ctx context.Context,
	client interfaces.MessageSender,
) {
	for {
		hook.askForMessages(ctx, client)
		time.Sleep(time.Second * checkDelay)
	}
}

func (hook Hook) askForMessages(
	ctx context.Context,
	client interfaces.MessageSender,
) {
	cli := &http.Client{}
	timeoutCtx, cancel := context.WithTimeout(ctx, time.Minute)
	defer cancel()
	req, _ := http.NewRequestWithContext(timeoutCtx, http.MethodGet, hook.URL, nil)
	resp, err := cli.Do(req)
	if err != nil {
		logger.LogInfo("HOOK ERROR: " + err.Error())

		return
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		logger.LogInfo("HOOK ERROR: " + string(body))

		return
	}

	if err != nil {
		logger.LogInfo("HOOK ERROR: " + err.Error())

		return
	}

	events := []EventForBot{}
	err = json.Unmarshal(body, &events)

	if err != nil {
		logger.LogInfo("HOOK ERROR: " + err.Error())

		return
	}

	for _, event := range events {
		if event.Type == TypeMessage {
			message := &telegram_api.SendMessageParams{
				ChatID: hook.Chat,
				Text:   event.Message,
			}

			logger.LogInfo("Send message " + event.Message + " to " + strconv.Itoa(int(hook.Chat)))

			_, err = client.SendMessage(timeoutCtx, message)
			if err != nil {
				logger.LogInfo("HOOK ERROR: " + err.Error())

				return
			}
		}
	}
}
