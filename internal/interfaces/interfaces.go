package interfaces

import (
	"context"

	telegram_api "github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
)

type MessageSender interface {
	SendMessage(context.Context, *telegram_api.SendMessageParams) (*models.Message, error)
}
