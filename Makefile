
init:
	go get -d ./...

lint: init
	golangci-lint run

build: init
	CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -o ./main -a -installsuffix cgo ./cmd/main.go

migrate:
	go install github.com/pressly/goose/v3/cmd/goose@latest
	goose -dir=migrations postgres "user=$(DB_LOGIN) password=$(DB_PASSWORD) dbname=$(DB_NAME) sslmode=disable" up

deploy:
	cp $(SETTINGS_JSON) ./settings.json
	sudo docker stop $(CI_PROJECT_NAME) || true && sudo docker rm $(CI_PROJECT_NAME) || true
	sudo docker build -t $(CI_PROJECT_NAME) -f Dockerfile .
	sudo docker run -d \
	    --name $(CI_PROJECT_NAME) \
	    -e BOT_TELEGRAM_BOT_TOKEN=$(BOT_TELEGRAM_BOT_TOKEN) \
	    $(CI_PROJECT_NAME)
	sudo docker network connect --alias $(CI_PROJECT_NAME) bot-network $(CI_PROJECT_NAME) --ip 172.18.0.3

cover:
	go test -short -count=1 -coverprofile=coverage.out ./...
	go tool cover -html=coverage.out
	rm coverage.out

test:
	go test -v -count=1 ./... -coverprofile=coverage.txt -covermode count
	go get github.com/boumenot/gocover-cobertura
	go run github.com/boumenot/gocover-cobertura < coverage.txt > coverage.xml
	go install gotest.tools/gotestsum@latest
	gotestsum --junitfile report.xml --format testname

mockgen:
	mockgen -source=internal/responses/responses.go -destination=internal/responses/responses_mock.go -package=responses
